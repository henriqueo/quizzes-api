package com.example.quizzesapi

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.validation.Valid
import javax.validation.constraints.*

@Entity
data class Quizz(
    @Id
    val id: UUID = UUID.randomUUID(),
    @ManyToMany
    val questions: MutableCollection<Question>,
//    val question: Question,
    @ManyToOne
    val user: User

)

@Repository
interface QuizzRepository : JpaRepository<Quizz, UUID> {
    fun findByUserId(userId: UUID): MutableList<Quizz>
}

data class QuizzReq(
//    @field:[NotNull]
//    val questionId: UUID,
    @field:[NotNull]
    val userId: UUID,
    @field:[NotNull]
    val questions: List<UUID>,
)

@RestController
@RequestMapping("quizz")
class QuizzController(
    val quizzRepository: QuizzRepository,
    val userRepository: UserRepository,
    val questionRepository: QuestionRepository
    ){

    @GetMapping
    fun index() = ResponseEntity.ok(quizzRepository.findAll())

    @GetMapping("{id}")
    fun retrieve(@PathVariable id: UUID): ResponseEntity<Quizz> {
        val quizz = getQuizz(id)

        return ResponseEntity.ok(quizz)
    }

    @PostMapping
    fun create(@Valid @RequestBody quizzReq: QuizzReq): ResponseEntity<Quizz>{
        val user = userRepository.findByIdOrNull(quizzReq.userId)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "User not found")

        var questions = mutableListOf<Question>()

        for(questionId in quizzReq.questions){
            questions.add(questionRepository.findByIdOrNull(questionId)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Question $questionId not found"))
        }

        val quizz = Quizz(
            user = user,
            questions=questions
        )

        quizzRepository.save(quizz)

        return ResponseEntity(quizz, HttpStatus.CREATED)
    }

    @PutMapping("{id}")
    fun update(@PathVariable id: UUID, @Valid @RequestBody quizzReq: QuizzReq): ResponseEntity<Quizz>{
        val quizz = getQuizz(id)

        val user = userRepository.findByIdOrNull(quizzReq.userId)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "User not found")

        var questions = mutableListOf<Question>()

        for(questionId in quizzReq.questions){
            questions.add(questionRepository.findByIdOrNull(questionId)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Question $questionId not found"))
        }

        val updatedQuizz = quizz.copy(
            user = user,
            questions=questions
        )

        quizzRepository.save(updatedQuizz)

        return ResponseEntity.ok(updatedQuizz)
    }

    @DeleteMapping("{id}")
    fun delete(@PathVariable id: UUID): ResponseEntity<Question>{
        val quizz = getQuizz(id)

        quizzRepository.delete(quizz)

        return ResponseEntity.noContent().build()
    }

    private fun getQuizz(id: UUID): Quizz{
        return quizzRepository.findByIdOrNull(id)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Quizz not found")
    }
}