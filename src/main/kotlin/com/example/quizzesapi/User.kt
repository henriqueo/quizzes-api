package com.example.quizzesapi

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.Valid
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
@Table(name = "users")
data class User(
    @Id
    val id: UUID = UUID.randomUUID(),
    val name: String,
    val email: String,
    val active: Boolean
)

@Repository
interface UserRepository : JpaRepository<User, UUID> {
    fun existsByEmail(email: String): Boolean
    fun existsByEmailAndIdNot(email: String, id: UUID): Boolean

}

data class UserDTO (
    @field:[NotNull NotBlank Size(min = 3, max = 20)]
    val name: String,

    @field:[Email]
    val email: String
)

@RestController
@RequestMapping("users")
class UserController(
    val userRepository: UserRepository,
    val quizzRepository: QuizzRepository
){

    @GetMapping
    fun index() = ResponseEntity.ok(userRepository.findAll())

    @PostMapping
    fun create(@Valid @RequestBody userReq: UserDTO): ResponseEntity<User> {
        if (userRepository.existsByEmail(userReq.email))
            throw ResponseStatusException(HttpStatus.CONFLICT, "E-mail already in use.")

        val user = User(name = userReq.name, email=userReq.email, active=true)

        userRepository.save(user)

        return ResponseEntity(user, HttpStatus.CREATED)
    }

    @GetMapping("{id}")
    fun retrieve(@PathVariable id: UUID): ResponseEntity<User> {
        val user = getUser(id)

        return ResponseEntity.ok(user)
    }

    @GetMapping("{userId}/quizzes")
    fun retrieveQuizzes(@PathVariable userId: UUID): ResponseEntity<MutableList<Quizz>> {
        val quizzes = getQuizzes(userId)

        return ResponseEntity.ok(quizzes)
    }

    @PutMapping("{id}")
    fun update(@PathVariable id: UUID, @Valid @RequestBody userReq: UserDTO): ResponseEntity<User> {
        if (userRepository.existsByEmailAndIdNot(userReq.email, id))
            throw ResponseStatusException(HttpStatus.CONFLICT, "E-mail already in use.")

        val user = getUser(id)

        val updatedUser = user.copy(name=userReq.name, email=userReq.email)

        userRepository.save(updatedUser)

        return ResponseEntity.ok(updatedUser)
    }

    @DeleteMapping("{id}")
    fun delete(@PathVariable id: UUID): ResponseEntity<User> {
        val user = getUser(id)

        userRepository.delete(user)

        return ResponseEntity.noContent().build()
    }

    private fun getUser(id: UUID): User{
        return userRepository.findByIdOrNull(id)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "User not found") // Elvis operator: se não for nulo...
    }

    private fun getQuizzes(userId: UUID): MutableList<Quizz>{
        getUser(userId)

        return quizzRepository.findByUserId(userId)
    }

}
